This should be all you need to set up a beer list that works with your database. There should be four new pages.

1. A tier 1 list (used for the display in the bar)
1. A tier 2 list (used for the display in the bar)
3. A a list with all beers (so people can view the beer list on their phones)
4. A printable list (So Anthony can print list to have in the bar)

You can view what the pages should look like in the ```/public/``` directory.

In the ```/template/``` directory you'll find the embedded ruby files. You'll have to change the erb in the html to whatever language you are using to interate over the database, but I left in the ruby to give you a starting point.

You may notice I query the database three times in ```/template/beer_list.html``` and twice in ```/template/tier_1_beer_list.html```. The reason is to put all the tier 1 beers first, then the tier 2, then the *'s.
